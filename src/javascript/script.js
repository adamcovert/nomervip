//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/jquery-migrate/dist/jquery-migrate.min.js

//= ../../node_modules/bootstrap/js/transition.js
//= ../../node_modules/bootstrap/js/modal.js

//= ../../node_modules/svg4everybody/dist/svg4everybody.min.js

//= ../..//node_modules/owl.carousel/dist/owl.carousel.min.js

// //= ../../node_modules/rellax/rellax.min.js
// //= ../../node_modules/aos/dist/aos.js

$(document).ready(function() {

  window.addEventListener('load', showContent);

  var loadTimer = setTimeout(function() {
    showContent();
    window.removeEventListener('load', showContent);
  }, 5000);

  function showContent(){
    clearTimeout(loadTimer);
    $('#preloader').fadeOut(500, function() {});
  }

  svg4everybody();

  $('#sales-slider').owlCarousel({
    dots: false,
    responsive: {
      0: {
        items: 1,
        margin: 40,
        stagePadding: 20,
        loop: true,
        autoplay: true,
        dots: true,
        autoplayTimeout: 2500
      },
      768: {
        items: 2,
        margin: 40,
        stagePadding: 20,
        loop: true,
        autoplay: true,
        dots: true,
        autoplayTimeout: 2500
      },
      992: {
        items: 3,
        margin: 40,
        stagePadding: 20,
        loop: false,
        nav: true,
        navText: ['<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#333" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#333" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
      },
      1200: {
        items: 3,
        margin: 40,
        stagePadding: 20,
        loop: true,
        nav: true,
        navText: ['<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#333" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#333" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
      }
    }
  });

  $('#steps-slider').owlCarousel({
    dots: false,
    responsive: {
      0: {
        items: 1,
        margin: 40,
        stagePadding: 20,
        loop: true,
        autoplay: true,
        dots: true,
        autoplayTimeout: 2500
      },
      448: {
        items: 2,
        margin: 40,
        stagePadding: 20,
        loop: true,
        autoplay: true,
        dots: true,
        autoplayTimeout: 2500
      },
      992: {
        items: 4,
        margin: 40,
        stagePadding: 20,
        loop: false,
        nav: true,
        mouseDrag: false,
        navText: ['<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#333" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#333" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
      }
    }
  });

  // Magic Button
  $(function() {
    $('.btn')
      .on('mouseenter', function(e) {
        var parentOffset = $(this).offset(),
            relX = e.pageX - parentOffset.left,
            relY = e.pageY - parentOffset.top;
        $(this).find('span').css({top:relY, left:relX})
      })
      .on('mouseout', function(e) {
        var parentOffset = $(this).offset(),
            relX = e.pageX - parentOffset.left,
            relY = e.pageY - parentOffset.top;
        $(this).find('span').css({top:relY, left:relX})
      });
    $('[href=#]').click(function(){return false});
  });

  // Responsive Menu
  var burger = document.querySelector('.burger');
  var menu = document.querySelector('.mobile-nav');
  var win = window;
  var body = document.querySelector('body');

  function openMenu(event) {
    menu.classList.toggle('mobile-nav--is-open');
    burger.classList.toggle('burger--is-active');
    body.classList.toggle('prevent-scroll');
    event.preventDefault();
    event.stopImmediatePropagation();
  }

  function closeMenu() {
    if ( menu.classList.contains('mobile-nav--is-open') ) {
      menu.classList.remove('mobile-nav--is-open');
      burger.classList.remove('burger--is-active');
      body.classList.remove('prevent-scroll');
    }
  }

  burger.addEventListener('click', openMenu, false);
  win.addEventListener('click', closeMenu, false);

});